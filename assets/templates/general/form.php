<!-- Form -->
<div class="form-group">
    <label for="websiteTitle">Titel</label>
    <input
        type="text"
        class="form-control"
        id="websiteTitle"
        name="websiteTitle"
        aria-describedby="websiteTitleHelp"
        placeholder="Titel der Webseite"
        value="<?= $_POST['websiteTitle'] ?? ''; ?>"
    >
    <small id="websiteTitleHelp" class="form-text text-muted">Der Titel, welchen Sie der Webseite geben möchten.</small>
</div>
<div class="form-group">
    <label for="websiteUserEmail">E-Mail Adresse</label>
    <input
        type="email"
        class="form-control"
        id="websiteUserEmail"
        name="websiteUserEmail"
        aria-describedby="websiteUserEmailHelp"
        placeholder="E-Mail eingeben"
        value="<?= $_POST['websiteUserEmail'] ?? ''; ?>"
    >
    <small id="websiteUserEmailHelp" class="form-text text-muted">Die primäre E-Mail Adresse des Systems</small>
</div>
<div class="form-group">
    <label for="websiteUser">Benutzername</label>
    <input
        type="text"
        class="form-control"
        id="websiteUser"
        name="websiteUser"
        aria-describedby="websiteUserHelp"
        placeholder="Benutzernamen eingeben"
        value="<?= $_POST['websiteUser'] ?? ''; ?>"
    >
    <small id="websiteUserHelp" class="form-text text-muted">Dein Admin Benutzername.</small>
</div>
<div class="form-group">
    <label for="websitePassword">Passwort</label>
    <input
        type="password"
        class="form-control"
        id="websitePassword"
        name="websitePassword"
        aria-describedby="websitePasswordHelp"
        placeholder="Passwort eingeben"
        value="<?= $_POST['websitePassword'] ?? ''; ?>"
    >
    <small id="websitePasswordHelp" class="form-text text-muted">Dein Admin Passwort.</small>
</div>