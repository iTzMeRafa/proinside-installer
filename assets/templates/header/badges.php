<!-- Badges -->
<div class="row">
    <div class="col-md-12">
        <div class="d-flex justify-content-center">
            <span class="badge badge-primary mr-2">Installer</span>
            <span class="badge badge-primary"><?= VERSION; ?></span>
        </div>
    </div>
</div>