<!-- Table -->
<div class="row">
    <div class="col-md-12">
        <table class="table">
            <tbody>
            <tr>
                <th scope="row">System OS</th>
                <td>
                    <div class="flexContainer">
                        <?= renderLamp(isValidServerOS()); ?>
                        <?= SERVER_OS; ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">PHP Version</th>
                <td>
                    <div class="flexContainer">
                        <?= renderLamp(isValidPHPVersion()); ?>
                        <?= PHP_VERSION; ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">Git installiert</th>
                <td>
                    <div class="flexContainer">
                        <?= renderLamp(gitExists()); ?>
                        <?= renderInstallText(gitExists()); ?>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>