<!-- Form -->
<div class="form-group">
    <label for="databaseName">Datenbank Namen</label>
    <input
            type="text"
            class="form-control"
            name="databaseName"
            id="databaseName"
            aria-describedby="databaseNameHelp"
            placeholder="Datenbank Namen eingeben"
            value="<?= $_POST['databaseName'] ?? ''; ?>"
    >
    <small id="emailHelpHelp" class="form-text text-muted">Der Namen der Datenbank, die du für ProInside verwenden möchtest.</small>
</div>
<div class="form-group">
    <label for="databaseUser">Benutzername</label>
    <input
        type="text"
        class="form-control"
        id="databaseUser"
        name="databaseUser"
        aria-describedby="databaseUserHelp"
        placeholder="Benutzernamen eingeben"
        value="<?= $_POST['databaseUser'] ?? ''; ?>"
    >
    <small id="databaseUserHelp" class="form-text text-muted">Dein Datenbank Benutzername.</small>
</div>
<div class="form-group">
    <label for="databasePassword">Passwort</label>
    <input
        type="password"
        class="form-control"
        id="databasePassword"
        name="databasePassword"
        aria-describedby="databasePasswordHelp"
        placeholder="Passwort eingeben"
        value="<?= $_POST['databasePassword'] ?? ''; ?>"
    >
    <small id="databasePasswordHelp" class="form-text text-muted">Dein Datenbank Passwort.</small>
</div>
<div class="form-group">
    <label for="databaseHost">Datenbank-Host</label>
    <input
        type="text"
        class="form-control"
        id="databaseHost"
        name="databaseHost"
        aria-describedby="databaseHostHelp"
        placeholder="localhost"
        value="<?= $_POST['databaseHost'] ?? ''; ?>"
    >
    <small id="databaseHostHelp" class="form-text text-muted">sollte "localhost" nicht existieren, erfrage bitte den korrekten Wert bei deinem Webhosting Anbieter.</small>
</div>