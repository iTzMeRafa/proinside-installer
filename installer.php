<?php declare(strict_types=1);
/**
 * PROINSIDE ONE-FILE-INSTALLER
 * --------------------------------------------------------------------------------------------------------------------
 * This is the automated proinside one-file-installer.
 * It will handle everything from cloning, parsing, setting up a clean database and adding
 * correct permissions on files and folders.
 * Also the static config files which are needed for this project are generated with correct values.
 */
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    echo "<pre>";
    var_dump($_POST);
    echo "</pre>";
}