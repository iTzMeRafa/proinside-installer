<?php declare(strict_types=1);
/**
 * PROINSIDE BOOTSTRAP
 * --------------------------------------------------------------------------------------------------------------------
 * This file defines all necessary data of the system and holds useful global functions for backend aswell as the
 * frontend.
 */

/**
 * Defines the operating system php is currently running on
 * @return 'Windows' | 'BSD' | 'Darwin' | 'OSX' | 'Solaris' | 'Linux' | 'Unknown'
 */
define('SERVER_OS', PHP_OS_FAMILY);

/**
 * Has the exact full name of the operating system php is running on
 * @return string
 */
define('SERVER_OS_FULL', php_uname());

/**
 * Gets the current year
 * @return string
 */
define ('CURRENT_YEAR', date('Y'));

/**
 * Current version of the installer
 */
define('VERSION', 'v0.1');

/**
 * Checks if a given command exists on the server
 * @param $cmd
 * @return bool
 */
function command_exist($cmd): bool {
    return !!`command -v $cmd`;
}

/**
 * Check if the OS is unix based, since bash commands wont work otherwise
 * @return bool
 */
function isValidServerOS(): bool {
    return SERVER_OS === 'Linux' || SERVER_OS === 'OSX' || SERVER_OS === 'Darwin' || SERVER_OS === 'Solaris' || SERVER_OS === 'BSD';
}

/**
 * Checks if the current PHP Version is 7 or higher which is needed for the system
 * @return bool
 */
function isValidPHPVersion(): bool {
    return intval(PHP_VERSION) >= 7;
}

/**
 * Checks if git is installed on the system
 * @return bool
 */
function gitExists(): bool {
    return command_exist('git');
}

/**
 * Checks if the requirements for the install are met and therefore the user is able to install
 * @return bool
 */
function isReadyForInstall(): bool {
    return
        isValidServerOS() &&
        isValidPHPVersion() &&
        gitExists()
    ;
}

/**
 * Returns text to show if a command is installed or not
 * @param $bool
 * @return string
 */
function renderInstallText($bool): string {
    return $bool
        ? 'installiert'
        : 'nicht installiert'
    ;
}

/**
 * Returns HTML of a lamp depending on the given bool
 * @param $bool
 * @return string
 */
function renderLamp($bool): string {
    return $bool
        ? '<div class="lamp lamp-green"></div>'
        : '<div class="lamp lamp-red"></div>'
    ;
}