<?php require_once('bootstrap.php'); ?>
<?php require_once('installer.php'); ?>

<!DOCTYPE html>
<html lang="de">

    <head>
        <meta charset="utf-8">
        <title>ProInside Installer</title>
        <meta name="author" content="Rafael Carneiro">
        <meta name="description" content="Installiert die ProInside Anwendung">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/vendor/bootstrap/bootstrap-4.4.css">
        <link href="assets/css/main.css" rel="stylesheet">
    </head>

    <body>

    <div class="row">
        <div class="container p-5">
            <div class="card">
                <div class="card-body">

                    <!-- Header -->
                    <section id="sectionHeader" class="sectionSpacing">
                        <?php require 'assets/templates/header/logo.php'; ?>
                        <?php require 'assets/templates/header/badges.php'; ?>
                    </section>

                    <!-- Requirements -->
                    <section id="sectionRequirements" class="sectionSpacing">
                        <div class="card">
                            <h5 class="card-header">System Voraussetzungen</h5>
                            <div class="card-body">
                                <?php require 'assets/templates/requirements/table.php'; ?>
                            </div>
                        </div>
                    </section>

                    <!-- BEGIN: FORM -->
                    <form method="POST" action="">

                        <!-- Database -->
                        <section id="sectionDatabase" class="sectionSpacing">
                            <div class="card">
                                <h5 class="card-header">Datenbank Informationen</h5>
                                <div class="card-body">
                                    <?php require 'assets/templates/database/form.php'; ?>
                                </div>
                            </div>
                        </section>

                        <!-- General -->
                        <section id="sectionGeneral" class="sectionSpacing">
                            <div class="card">
                                <h5 class="card-header">Allgemeine Informationen</h5>
                                <div class="card-body">
                                    <?php require 'assets/templates/general/form.php'; ?>
                                </div>
                            </div>
                        </section>

                        <!-- Installieren Button -->
                        <section id="sectionInstallButton" class="sectionSpacing">
                            <?php require 'assets/templates/installButton/button.php'; ?>
                        </section>

                    </form>
                    <!-- END: FORM -->

                    <!-- Footer -->
                    <section id="footer">
                        Copyright <?= CURRENT_YEAR; ?> - <?= CURRENT_YEAR +1; ?>. Alle Rechte vorbehalten. &copy; <strong>ProInside</strong>
                    </section>

                </div>
            </div>
        </div>
    </div>

    <script src="assets/vendor/jquery/jquery-3.4.1.js"></script>
    <script src="assets/vendor/bootstrap/popper-1.16.0.js"></script>
    <script src="assets/vendor/bootstrap/bootstrap-4.4.1.js"></script>
    </body>

</html>